provider "local" {
}

resource "local_file" "test" {
  ilename = "/tmp/this_will_fail_anyway" // <- "ilename" instead of "filename"
  content  = "Terraform role test file"
}
