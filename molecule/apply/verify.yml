---
- name: Verify
  hosts: all
  gather_facts: true

  pre_tasks:
    - name: Current user should be ansible
      ansible.builtin.assert:
        that:
          - ansible_user_id == "ansible"

  tasks:
    - name: Expected output file should exist
      block:
        - name: Stat expected output file
          ansible.builtin.stat:
            path: "{{ test_terraform_output_file }}"
          register: tmp_terraform_outputfile_stat_result
          failed_when:
            - not tmp_terraform_outputfile_stat_result.stat.exists

    - name: <apply> command should fail if no argument is provided
      block:
        - name: Exercice <apply> command
          ansible.builtin.import_role:
            name: glienhart.terraform
            tasks_from: "apply"

        - name: This task should not run
          ansible.builtin.fail:
            msg: "This task should not run"
          register: test_terraform_should_not_be_defined
      rescue:
        - name: <apply> command should have failed
          ansible.builtin.assert:
            that: test_terraform_should_not_be_defined is not defined

        - name: Role argument validation should complain about required arguments missing
          ansible.builtin.assert:
            that:
              - ansible_failed_task.action == "ansible.builtin.validate_argument_spec"
              - ansible_failed_result.argument_errors is defined
              - (item.key in ansible_failed_result.argument_errors | first)
          loop: "{{ ansible_failed_result.argument_spec_data | dict2items }}"
          when: item.value.required

    - name: <apply> command should fail when applying an invalid plan
      block:
        - name: Exercice <apply> command
          ansible.builtin.import_role:
            name: glienhart.terraform
          vars:
            terraform_action: "apply"
            terraform_plan_dir: "./files/invalid_terraform_plan"

        - name: This task should not run
          ansible.builtin.fail:
            msg: "This task should not run"
          register: test_terraform_should_not_be_defined
      rescue:
        - name: <apply> command should have failed
          ansible.builtin.assert:
            that: test_terraform_should_not_be_defined is not defined

        - name: <apply> command should fail at validation step
          ansible.builtin.assert:
            that:
              - last_unskipped_result.failed is defined
              - last_unskipped_result.failed == true
              - last_unskipped_result.tmp_terraform_command == "validate"
          vars:
            last_unskipped_result: >
              {{
                (
                  (ansible_failed_result.results | selectattr('skipped', 'undefined') | list)
                  +
                  (ansible_failed_result.results | selectattr('skipped', 'defined') | rejectattr('skipped', 'true') | list)
                )
                | last
              }}
