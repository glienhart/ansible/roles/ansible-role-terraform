# Terraform - Ansible role

An Ansible role to install and run [HashiCorp Terraform](https://www.terraform.io).

> :construction: **WIP**
>
> This role is still under construction and is not ready for production.

## Supported platforms

The role currently supports the Linux distributions below:

- [Debian 12.4](https://www.debian.org)
- [Ubuntu 22.04](https://ubuntu.com)

It may work on other releases but this has not being tested yet.

## Entry points

The role has several entry points:

- `install`

  Install Terraform from Hashicorp package repository.

- `apply`

  Apply a Terraform plan.

## Role Variables

A detailed description of each entry point parameters is available
in the [argument_specs.yml](./meta/argument_specs.yml) file.

## Requirements

The role has the following dependencies:

- gpg

## Unit tests

The role source code is validated with
[Ansible Lint](https://ansible.readthedocs.io/projects/lint/)
and covered by [unit tests](./molecule) written with
[Ansible Molecule](https://ansible.readthedocs.io/projects/molecule).

Tests are executed in Docker containers.

They run in a
[CI/CD pipeline](https://gitlab.com/glienhart/ansible/roles/ansible-role-terraform/-/pipelines)
each time a change is pushed to the repository.


They can be run locally with:

```
molecule test --all
```

Output excerpt:

```
PLAY RECAP *********************************************************************
terraform-install-debian12.4 : ok=5    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
terraform-install-ubuntu22.04 : ok=5    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

PLAY RECAP *********************************************************************
terraform-apply-debian12.4 : ok=13   changed=0    unreachable=0    failed=0    skipped=0    rescued=2    ignored=0
terraform-apply-ubuntu22.04 : ok=13   changed=0    unreachable=0    failed=0    skipped=0    rescued=2    ignored=0
```

## Dependencies

This role depends on:

| Role name         | Repository                    |
| ---               | ---                           |
| `glienhart.app`   | glienhart/ansible/roles/ansible-role-app>  |

## Example Playbook

```
- hosts: all
  roles:
    - name: Install Terraform
      role: glienhart.terraform
      terraform_action: "install"
      terraform_version: "1.7.2"

    - name: Apply Terraform test plan
      role: glienhart.terraform
      terraform_action: "apply"
      terraform_plan_dir: "./files/terraform_test_plan"
```

Note that the name of the entry point to be called is provided via
the `terraform_action` argument.

Examples for each entry point can be found in unit tests converge files:

* [molecule/install/converge.yml](molecule/install/converge.yml)
* [molecule/apply/converge.yml](molecule/apply/converge.yml)

> :bulb: **TIP**
>
> Check out glienhart/ansible/ansible-roles-usage-example> repository
> for a **full example of this role usage**.

## License

MIT

## Author Information

This role was created by [Geoffrey Lienhart](https://www.linkedin.com/in/geoffrey-lienhart-41462870).
